# README for the Frontend of tuency

## General
The main development setup is documented in the projects [README](../README.md).  
For interacting with the client containers npm and yarn tools please read the instructions for the [dev-util](../docs/dev-util.md) script.

## Configuration

Each client instance provides several theming and configuration files under the webservers `<client-url>/resources` path.  
Examples of these files can be found in [tuency resouce examples](../docs/examples/tuency_resources).

### Keycloak

In the `keycloak` section of `/resources/client-config.json` you can specify to which url,
realm and client-id the respective client instance has to connect.  
NOTE: The client application (vue app) won't load if this file is not present.

```json
{
    "keycloak": {
        "url": "http://keycloak.tuency.localhost",
        "realm": "tuency",
        "clientId": "tuencyone"
    }
}
````

### Theming
Each client needs to provide the following style files and logo directory under it's webservers `/resources` path:  

- **client_style.css** *- Tuency client color theming)*
- **login_style.css** *- Client login style theming*
- **account_style.css** *- Keycloak account ui style currently not working see [News v2.0.0](../NEWS.md)*
- **logo.png** *- The clients main logo (left top)*
- **/logo/logo-<tenant-name>.svg** *- Each clients svg logo for constituency preview in tables*

> :warning: It is necessary that your SVG files contain the [`viewBox`](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/viewBox) attribute to ensure that the browser can scale and display them correctly.

## Third party code, licensing info

The initial base of the frontend was created 2020-09 with `vue/cli create`.
[vue-cli](https://github.com/vuejs/vue-cli) is licenced under
[MIT](../LICENSES/MIT.xt) and

    Copyright (c) 2017-2020, Yuxi (Evan) You
