/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author(s):
 * 2021, 2022 Fadi Abbud <fadi.abbud@intevation.de>
 */
import { ref } from "vue";
export function useUserInfo() {
  const userId = ref("");
  const showUserInfo = ref(false);
  const checkUserChildObject = e => {
    if (e.closeDialog) {
      showUserInfo.value = false;
    }
  };
  const formNodes = groups => {
    let nodes;
    nodes = [];
    groups.forEach(g => {
      if (g && g.nodes && g.nodes.length) {
        nodes = nodes.concat(g.nodes.map(n => n.name));
      }
    });
    if (nodes.length) {
      return nodes.join(", ");
    } else {
      return "";
    }
  };
  const formRoles = groups => {
    const roles: string[] = [];
    groups.forEach(g => {
      roles.push(g.level);
    });
    return roles;
  };
  return { userId, showUserInfo, checkUserChildObject, formNodes, formRoles };
}
