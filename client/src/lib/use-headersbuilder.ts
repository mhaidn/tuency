import store from "@/store";

// Adjust the table columns according to the user
export const useHeadersBuilder = headers => {
  const hdr = headers.filter((header) => header.hidden !== true);

  if (!store.getters["user/isPortalAdmin"]) {
    return hdr;
  } else {
    const extraColumn = {
      text: "Updated by",
      value: "updated_by",
      sortable: true,
      divider: true
    };
    // Copy array
    const headers = hdr.filter(r => r); // copy array, don't mutate original
    headers.splice(
      headers.indexOf(h => h === "actions"),
      0,
      extraColumn
    );
    return headers;
  }
};
