<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RuleFormRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $organisation = $this->route('organisation');

        return [
            'classification_taxonomy_id' =>
            'exists:classification_taxonomy,classification_taxonomy_id|nullable',
            'classification_type_id' =>
            'exists:classification_type,classification_type_id|nullable',
            'classification_identifier' => 'nullable|string|max:100',
            'feed_provider_id' => 'exists:feed_provider,feed_provider_id|nullable',
            'feed_name_id' => 'exists:feed_name,feed_name_id|nullable',
            'feed_status' => 'in:production,beta|nullable',
            'abuse_c' => 'boolean',
            'suppress' => 'boolean',
            'interval_length' => 'integer|min:0',
            'interval_unit' => 'in:immediate,hours,days,weeks,month',
            'commentary' => 'nullable|string|max:255',

            // Contacts are given as an array of contact IDs. They must refer
            // to contacts associated with the organisation referenced in the
            // request URI.
            'contacts' => 'array',
            'contacts.*' => [
                'integer',
                Rule::exists('contact', 'contact_id')
                    ->where(function ($query) use ($organisation) {
                        return $query->where(
                            'organisation_id',
                            $organisation->organisation_id
                        );
                    }),
            ],
        ];
    }

    /**
     * Get the validated data from the request, with defaults added.
     *
     * Missing selector data is replaced with defaults meaning "any".
     * @param null $key
     * @param null $default
     */
    public function validated($key = null, $default = null)
    {
        $validated = parent::validated();

        $validated['classification_taxonomy_id']
            = $validated['classification_taxonomy_id'] ?? -1;
        $validated['classification_type_id']
            = $validated['classification_type_id'] ?? -1;
        $validated['feed_provider_id']
            = $validated['feed_provider_id'] ?? -1;
        $validated['feed_name_id']
            = $validated['feed_name_id'] ?? -1;
        $validated['feed_status']
            = $validated['feed_status'] ?? 'any';
        $validated['classification_identifier'] = trim($validated['classification_identifier'] ?? '');

        return $validated;
    }
}
