<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Magnus Schieder <magnus.schieder@intevation.de>
 */

namespace App\Http\Controllers;

use App\Models\Organisation;
use App\Models\Contact;
use App\Models\ContactTag;
use App\Models\OrganisationTag;
use App\Models\PDF;
use App\Models\User;
use App\Services\TuencyConfigService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    private TuencyConfigService $configService;
    private $contactRoles;
    private $contactFields;
    private $contactsFieldsValidation;
    private $dropdownFields;
    private $searchedFields;
    private $dropDownKeyMap;

    public function __construct(TuencyConfigService $configService)
    {
        $this->configService = $configService;

        $contactRoles = $this->configService->getContactRoles();
        if (empty($contactRoles)) {
            Log::error("tuency.contact_roles not set");
            abort(500);
        }

        $contactFields = $this->configService->getContactFields();
        if (empty($contactFields)) {
             Log::error("tuency.contact_fields not set");
             abort(500);
        }

        $contactsFieldsValidation = [];
        $dropdownFields = [];
        $searchedFields = [];
        foreach ($contactFields as $fieldName => $properties) {
            if (array_key_exists('dropdown', $properties)) {
                $dropdownFields[$fieldName] = $properties['dropdown'];
                $contactsFieldsValidation[$fieldName] = [
                    'nullable',
                    Rule::in(array_keys($properties['dropdown'])),
                ];
                // Apply searchable dropdowns to search fields and reversing entries key <=> value to lower for lookup.
                if (array_key_exists('searchable', $properties) && $properties['searchable'] === true) {
                    $searchedFields[] = $fieldName;
                    $this->dropDownKeyMap[$fieldName] = array_change_key_case(
                        array_flip($properties['dropdown']),
                        CASE_LOWER
                    );
                }
            } elseif (
                $properties['type'] === 'text' ||
                $properties['type'] === 'text_area' ||
                $properties['type'] === 'phone'
            ) {
                $contactsFieldsValidation[$fieldName] = 'string|nullable';
                array_push($searchedFields, $fieldName);
            } elseif ($properties['type'] === 'integer') {
                $contactsFieldsValidation[$fieldName] = 'integer|nullable';
                array_push($searchedFields, $fieldName);
            } elseif ($properties['type'] === 'email') {
                $contactsFieldsValidation[$fieldName] = 'email|nullable';
                array_push($searchedFields, $fieldName);
            } elseif ($properties['type'] === 's_mime_cert' || $properties['type'] === 'openpgp_pubkey') {
                $contactsFieldsValidation[$fieldName] = 'string|nullable';
            } elseif ($properties['type'] === 'pdf') {
                $contactsFieldsValidation[$fieldName] = 'file|mimes:pdf|nullable';
            }
        };

        $this->contactRoles = $contactRoles;
        $this->contactFields = $contactFields;
        $this->contactsFieldsValidation = $contactsFieldsValidation;
        $this->dropdownFields = $dropdownFields;
        $this->searchedFields = $searchedFields;
    }

    /**
     * Create a pattern for LIKE that matches a given substring.
     *
     * This method replaces '_' with '\_' and '%' with '\%' so that the
     * database does not interpret them as wild-cards and then surrounds
     * the result with '%' wild-cards so that it matches a sub-string.
     */
    private function substringPattern(string $literal): string
    {
        $escaped = str_replace('_', '\\_', str_replace('%', '\\%', $literal));
        return "%$escaped%";
    }

    /**
     * Checks if the search input can be mapped to a searchable dropdown.
     * If yes, an array with the field key and respective dropdown key is returned.
     * If not, an empty array is returned.
     *
     * @param string $search
     * @return array
     */
    private function detectDropDownSearch(string $search): array
    {
        if (empty($search)) {
            return [];
        }

        $search = strtolower($search);
        $matches = [];

        foreach ($this->dropDownKeyMap as $fieldName => $fieldSelectSet) {
            foreach (array_keys($fieldSelectSet) as $fieldSelectKey) {
                if (str_contains($fieldSelectKey, $search)) {
                    $matches[] = [
                        'field' => $fieldName,
                        'value' => $fieldSelectSet[$fieldSelectKey]
                    ];
                }
            }
        }

        return $matches;
    }

    /*
     * Retrieve all contacts that the user is allowed to see.
     *
     * If the optional query parameter 'page' is given the result is
     * paginated using Laravel's standard pagination. The value of the
     * page parameter should be an integer. Page numbering starts from 1.
     *
     * The parameter 'sort_by' can be used to specify the field by which the
     * contacts should be sorted.
     *
     * The parameter 'row' specifies how many entries are to be displayed per
     * page. If "row" is not specified, the default value 10 is used.
     *
     * With the optional query parameter 'ancestor', which must be the ID of an
     * organisation if specified, the result is restricted to contacts from that
     * organisation and its direct and indirect sub-organisations.
     *
     * With the optional query parameter 'tenant', which must be the ID of a
     * tenant if specified, the result is restricted to contacts of
     * organisations that are connected to this tenant directly or indirectly
     * through their parents.
     */

    public function contacts(Request $request)
    {
        $validated = $request->validate([
            'page' => 'int|nullable',
            'sort_by' => 'string|nullable|in:last_name,first_name,contact_id',
            'row' => 'int|nullable',
            'ancestor' => 'int|nullable',
            'tenant' => 'int|nullable',
            'tag' => [
                'integer',
                function ($attribute, $value, $fail) {
                    if (is_null(ContactTag::forUser(Auth::user())->find($value))) {
                        $fail('Unknown contact tag');
                    }
                }
            ],
            'organisation_tag' => [
                'integer',
                function ($attribute, $value, $fail) {
                    if (is_null(OrganisationTag::forUser(Auth::user())->find($value))) {
                        $fail('Unknown organisation tag');
                    }
                }
            ],
            'roles' => 'array|nullable',
            'search' => 'string|nullable',
            'expired' => 'boolean|nullable',
            'organisation' => 'int|nullable',
            'contact_id' => 'int|nullable',
        ]);

        $this->logRequest($validated);

        $page = $validated['page'] ?? -1;
        $sortBy = $validated['sort_by'] ?? "last_name";
        $row = $validated['row'] ?? 10;
        $tag = $validated['tag'] ?? -1;
        $roles = $validated['roles'] ?? null;
        $search = $validated['search'] ?? '';
        $expiredOnly = $validated['expired'] ?? false;
        $contactId = $validated['contact_id'] ?? null;

        // Determine IDs of the organisations whose contacts we want
        $orgaQuery = Organisation::querySubHierarchy(
            Auth::user(),
            $validated['ancestor'] ?? null,
            $validated['tenant'] ?? null,
        );
        if (array_key_exists('organisation', $validated)) {
            $orgaQuery = $orgaQuery->where(
                'organisation.organisation_id',
                '=',
                $validated['organisation']
            );
        }
        if (array_key_exists('organisation_tag', $validated)) {
            $orgaQuery = $orgaQuery->whereHas('tags', function ($query) use ($validated) {
                $query->whereKey($validated['organisation_tag']);
            });
        }

        // Build contact query
        $query = Contact::whereIn(
            'organisation_id',
            $orgaQuery->pluck('organisation.organisation_id')
        );

        // Apply role restriction condition
        $this->applyClientRoleRestrictionToContactQuery($query, Auth::user()->getGroups());

        if ($contactId) {
            $query->where('contact_id', $contactId);
        }

        if ($tag >= 0) {
            $query = $query->forTag($tag);
        }

        if ($search !== '') {
            $query = $query->where(function ($query) use ($search) {

                $pattern = $this->substringPattern($search);

                // Fulltext search
                foreach ($this->searchedFields as $searchedField) {
                    $query->orWhere('contact.' . $searchedField, 'ilike', $pattern);
                }

                // Full name
                $query->orWhereRaw("CONCAT(contact.first_name, ' ', contact.last_name) LIKE ?", $pattern)
                    ->orWhereRaw("CONCAT(contact.last_name, ' ', contact.first_name) LIKE ?", $pattern);

                // Searchable dropdown
                if ($dropDownQueries = $this->detectDropDownSearch($search)) {
                    foreach ($dropDownQueries as $dropDownQuery) {
                        $query->orWhere('contact.' . $dropDownQuery['field'], 'ilike', $dropDownQuery['value']);
                    }
                }
            });
        }

        if ($expiredOnly) {
            $query->where('updated_at', '<=', $this->configService->getCurrentContactExpiryDate());
        }

        $query->with([
            'tags' => function ($query) {
                $query->forUser(Auth::user());
            },
        ]);

        if (!empty($roles)) {
            $query->where(function ($query) use ($roles) {
                $query->whereJsonContains('roles', $roles[0]);

                for ($i = 1; $i <= count($roles) - 1; $i++) {
                    $query->orWhereJsonContains('roles', $roles[$i]);
                }
            });
        }

        if ($sortBy === "last_name") {
             $query = $query->orderBy("last_name");
             $query = $query->orderBy("first_name");
        } elseif ($sortBy === "first_name") {
             $query = $query->orderBy("first_name");
             $query = $query->orderBy("last_name");
        }
        $query = $query->orderBy("contact_id");


        // Perform query
        if ($page > 0) {
            $contacts = $query->paginate($row, ['*'], 'page', $page);
        } else {
            $contacts = $query->get();
        }

        // Postprocess results
        foreach ($contacts as $contact) {
            foreach ($this->dropdownFields as $fieldName => $dropdownOption) {
                if ($value = $contact[$fieldName]) {
                    $contact[$fieldName] = [
                        'label' => $dropdownOption[$value],
                        'value' => $value
                    ];
                } else {
                    $contact[$fieldName] = (object)[];
                }
            }

            $contact->organisation_name = $contact->organisation()->value('name');

            if ($pdfs = $contact->pdf()->get()) {
                foreach ($pdfs as $pdf) {
                    $contact[$pdf->type] = [
                        'name' => $pdf->name,
                        'id' => $pdf->pdf_id,
                    ];
                }
            }
        }

        return $contacts;
    }

    /**
     * Retrieve all contacts of the organization.
     */

    public function index(Organisation $organisation)
    {
        $this->logRequest();

        $contacts = $organisation->contacts()->with([
            'tags' => function ($query) {
                $query->forUser(Auth::user());
            },
        ])->where(function ($query) {
            $query->whereJsonLength('restrict_to_client_roles', 0)->orWhere(function ($query) {
                $clientRoles = Auth::user()->getGroups();
                $query->whereJsonContains('restrict_to_client_roles', $clientRoles[0]);

                for ($i = 1; $i <= count($clientRoles) - 1; $i++) {
                    $query->orWhereJsonContains('restrict_to_client_roles', $clientRoles[$i]);
                }
            });
        })->get();

        foreach ($contacts as $contact) {
            foreach ($this->dropdownFields as $fieldName => $dropdownOption) {
                if ($value = $contact[$fieldName]) {
                    $contact[$fieldName] = [
                        'label' => $dropdownOption[$value],
                        'value' => $value
                    ];
                } else {
                    $contact[$fieldName] = (object)[];
                }
            }

            $contact->organisation_name = $contact->organisation()->value('name');

            if ($pdfs = $contact->pdf()->get()) {
                foreach ($pdfs as $pdf) {
                    $contact[$pdf->type] = [
                        'name' => $pdf->name,
                        'id' => $pdf->pdf_id,
                    ];
                }
            }
        }
        return $contacts;
    }

    /**
     * Create a new contact associated with an organisation.
     */
    public function store(Request $request, Organisation $organisation)
    {
        $contactsFieldsValidation = $this->contactsFieldsValidation;
        $contactsFieldsValidation['tags.*'] = ['integer', Rule::exists('contact_tag', 'contact_tag_id')];
        $contactsFieldsValidation['roles'] = 'required|array';
        $contactsFieldsValidation['roles.*'] = [
            'string',
            Rule::in($this->getAllowedRolesByOrganisation($organisation))
        ];
        $contactsFieldsValidation['restrict_to_client_roles'] = 'nullable|array';
        $contactsFieldsValidation['restrict_to_client_roles.*'] = ['string', Rule::in(Auth::user()->getGroups())];

        $validator = Validator::make($request->all(), $contactsFieldsValidation);

        $createArray = [];
        $createArrayPDF = [];
        $validator->after(function ($validator) use (&$createArray, &$createArrayPDF, &$organisation) {
            $validated = $validator->validated();

            # Merge the fields de contact rolls. If a field is required, it
            # should remain required after the merge.
            $fieldsValidation = [];
            foreach ($validated['roles'] as $role) {
                foreach ($this->contactRoles[$role] as $fieldName => $required) {
                    if (!array_key_exists($fieldName, $fieldsValidation) || $required) {
                        $fieldsValidation[$fieldName] = $required;
                    }
                }
            }

            foreach (array_keys($this->contactsFieldsValidation) as $fieldName) {
                if (array_key_exists($fieldName, $fieldsValidation)) {
                    if (array_key_exists($fieldName, $validated)) {
                        if ($this->contactFields[$fieldName]['type'] !== 'pdf') {
                            $createArray[$fieldName] = $validated[$fieldName];
                        } else {
                            $createArrayPDF[$fieldName] = $validated[$fieldName];
                        }
                    } else {
                        # Add an error if field is not specified and required.
                        if ($fieldsValidation[$fieldName]) {
                            $validator->errors()->add(
                                $fieldName,
                                'Is required for the given contact rolls.'
                            );
                        }
                    }
                } else {
                    # Add an error if a field was given that was not allowed to
                    # be given.
                    if (array_key_exists($fieldName, $validated)) {
                        $validator->errors()->add(
                            $fieldName,
                            'Is not permitted for the given contact rolls.'
                        );
                    }
                }
            }

            // Organisation mail domains
            if ($contactMail = $validator->getValue('email')) {
                if (! $this->emailMatchesOrganisation($contactMail, $organisation->organisation_id)) {
                    $validator->errors()->add(
                        'email',
                        'Is not in the organisations mail domains.'
                    );
                }
            }
        });

        // Explicit file extension check for pdf documents to be saved.
        foreach ($createArrayPDF as $pdfType => $pdf) {
            if (! $this->hasPdfFileExtension($pdf->getClientOriginalName())) {
                $validator->errors()->add(
                    $pdfType,
                    'Is not a valid pdf.'
                );
            }
        }

        $validated = $validator->validate();

        return DB::transaction(function () use ($validated, $organisation, $createArray, $createArrayPDF) {
            $createArray['organisation_id'] = $organisation->organisation_id;
            $createArray['roles'] = json_encode($validated['roles']);
            $createArray['restrict_to_client_roles'] = json_encode($validated['restrict_to_client_roles'] ?? []);

            $this->logRequest($createArray);

            $contact = Contact::create($createArray);

            if (Gate::allows('assign-contact-tags') && array_key_exists('tags', $validated)) {
                $this->syncTags($organisation, $contact, Auth::user(), $validated['tags']);
            }

            foreach ($createArrayPDF as $pdfType => $pdf) {
                $newPdf = [
                    'name' => $this->getRandomizedPfdFilename($pdfType),
                    'pdf' => base64_encode(file_get_contents($pdf->getRealPath())),
                    'type' => $pdfType,
                ];

                $contact->pdf()->create($newPdf);
            }
            return $contact;
        });
    }

    /**
     * Retrieve a specific contact.
     */
    public function show(Organisation $organisation, Contact $contact)
    {
        $this->logRequest();

        if (! $this->userIsAuthorizedToContact($contact)) {
            abort(403, 'Unauthorized action.');
        }

        foreach ($this->dropdownFields as $fieldName => $dropdownOption) {
            if ($value = $contact[$fieldName]) {
                $contact[$fieldName] = [
                    'label' => $dropdownOption[$value],
                    'value' => $value
                ];
            } else {
                $contact[$fieldName] = (object)[];
            }
        }
        if ($pdfs = $contact->pdf()->get()) {
            foreach ($pdfs as $pdf) {
                $contact[$pdf->type] = [
                    'name' => $pdf->name,
                    'id' => $pdf->pdf_id,
                ];
            }
        }
        return $contact;
    }

    /**
     * Update a contact.
     */
    public function update(Request $request, Organisation $organisation, Contact $contact)
    {
        $this->logRequest();

        if (! $this->userIsAuthorizedToContact($contact)) {
            abort(403, 'Unauthorized action.');
        }

        $isOrganisationUpdate = $request->get('organisation_id') != $organisation->id;

        $contactsFieldsValidation = $this->contactsFieldsValidation;
        $contactsFieldsValidation['tags.*'] = ['integer', Rule::exists('contact_tag', 'contact_tag_id')];
        $contactsFieldsValidation['roles'] = 'required|array';
        $contactsFieldsValidation['roles.*'] = [
            'string',
            Rule::in($this->getAllowedRolesByOrganisation($isOrganisationUpdate ?
                Organisation::find($request->get('organisation_id')) :
                $organisation))
        ];
        $contactsFieldsValidation['restrict_to_client_roles'] = 'nullable|array';
        $contactsFieldsValidation['restrict_to_client_roles.*'] = ['string', Rule::in(Auth::user()->getGroups())];
        $contactsFieldsValidation['organisation_id'] = [
            'integer',
            Rule::exists('organisation', 'organisation_id'),
            Rule::prohibitedIf(
                $isOrganisationUpdate && $contact->is_rule_destination
            )
        ];

        $validator = Validator::make($request->all(), $contactsFieldsValidation);

        $updateArray = [];
        $updateArrayPDF = [];
        $validator->after(function ($validator) use ($contact, &$updateArray, &$updateArrayPDF) {
            $validated = $validator->validated();

            # Merge the fields de contact rolls. If a field is required, it
            # should remain required after the merge.
            $fieldsValidation = [];
            foreach ($validated['roles'] as $role) {
                foreach ($this->contactRoles[$role] as $fieldName => $required) {
                    if (!array_key_exists($fieldName, $fieldsValidation) || $required) {
                        $fieldsValidation[$fieldName] = $required;
                    }
                }
            }

            foreach (array_keys($this->contactsFieldsValidation) as $fieldName) {
                if (array_key_exists($fieldName, $fieldsValidation)) {
                    if (array_key_exists($fieldName, $validated)) {
                        if ($this->contactFields[$fieldName]['type'] !== 'pdf') {
                            # If the value in the field has been deleted, set the
                            # default value based on the field type for the DB.
                            if ($validated[$fieldName] === null) {
                                if (
                                    in_array(
                                        $this->contactFields[$fieldName]['type'],
                                        ['text',
                                        'text_area',
                                        'phone',
                                        's_mime_cert',
                                        'openpgp_pubkey']
                                    )
                                ) {
                                    $updateArray[$fieldName] = '';
                                } elseif ($this->contactFields[$fieldName]['type'] === 'integer') {
                                    $updateArray[$fieldName] = -1;
                                }
                            } else {
                                $updateArray[$fieldName] = $validated[$fieldName];
                            }
                        } else {
                            $updateArrayPDF[$fieldName] = $validated[$fieldName];
                        }
                    } else {
                        # Add an error if field is not specified and required.
                        # If the PDF is already in the database, it is not required.
                        if (
                            $fieldsValidation[$fieldName] &&
                            $contact->pdf()->where('type', $fieldName)->doesntExist()
                        ) {
                            $validator->errors()->add(
                                $fieldName,
                                'Is required for the given contact rolls.'
                            );
                        }
                    }
                } else {
                    # Add an error if a field was given that was not allowed to
                    # be given.
                    if (array_key_exists($fieldName, $validated)) {
                        $validator->errors()->add(
                            $fieldName,
                            'Is not permitted for the given contact rolls.'
                        );
                    }
                }
            }

            // Organisation mail domains
            if ($contactMail = $validator->getValue('email')) {
                if (! $this->emailMatchesOrganisation($contactMail, $validator->getValue('organisation_id'))) {
                    $validator->errors()->add(
                        'email',
                        'Is not in the organisations mail domains.'
                    );
                }
            }
        });

        // Explicit file extension check for pdf documents to be saved.
        foreach ($updateArrayPDF as $pdfType => $pdf) {
            if (! $this->hasPdfFileExtension($pdf->getClientOriginalName())) {
                $validator->errors()->add(
                    $pdfType,
                    'Is not a valid pdf.'
                );
            }
        }

        $validated = $validator->validate();

        return DB::transaction(function () use ($validated, $organisation, $contact, $updateArray, $updateArrayPDF) {
            $updateArray['roles'] = json_encode($validated['roles']);

            if ($validated['organisation_id'] != $organisation->id) {
                $updateArray['organisation_id'] = $validated['organisation_id'];
            }

            // Detect fields that are not required in the current role set and clear them.
            foreach ($this->contactFields as $fieldKey => $fieldAttributes) {
                if (! array_key_exists($fieldKey, $updateArray) && $fieldAttributes['type'] !== 'pdf') {
                    $updateArray[$fieldKey] = null;
                }
            }

            $this->logRequest($updateArray);

            $contact->touch();
            $contact->update($updateArray);

            if (Gate::allows('assign-contact-tags')) {
                $this->syncTags($organisation, $contact, Auth::user(), $validated['tags'] ?? []);
            }

            foreach ($updateArrayPDF as $pdfType => $pdf) {
                $contact->pdf()->updateOrCreate(
                    ['type' => $pdfType],
                    [
                        'name' => $this->getRandomizedPfdFilename($pdfType),
                        'pdf' => base64_encode(file_get_contents($pdf->getRealPath())),
                        'type' => $pdfType,
                    ],
                );
            }

            // Remove pdfs that are no longer required by current role set.
            foreach ($contact->pdf()->get(['pdf_id', 'type']) as $pdf) {
                $keepPdf = false;
                foreach (json_decode($contact->roles) as $role) {
                    if (array_key_exists($pdf->type, $this->contactRoles[$role])) {
                        $keepPdf = true;
                    }
                }

                if (! $keepPdf) {
                    $pdf->delete();
                }
            }

            return $contact;
        });
    }

    /**
     * Update the contact's tags so that user may assign to the tags in
     * $desiredTags
     *
     * Replace the subset of $contact's tags that $user has permissions for
     * with those in $desiredTags
     *
     * The semantics are somewhat complicated here. The user can usually only
     * work with a subset of all possible tags (e.g. for tenant admins only
     * those associated with the right tenants) but the contact may already
     * have tags that are outside that subset and those tags must not be
     * changed by this method. The new set of tags of the contact is
     * effectively
     *
     *    (OLD - ASSIGNABLE) + (DESIRED & ASSIGNABLE)
     *
     * where
     *
     *    OLD is the set of tags of the contact before this method is called
     *
     *    ASSIGNABLE is the set of tags the user has permissions for and that
     *    belong to a tenant the organisation belongs to
     *
     *    DESIRED is the set of tags in $desiredTags
     *
     *    - is set difference, + is set union and & is set intersection
     */
    public function syncTags(
        Organisation $organisation,
        Contact $contact,
        User $user,
        $desiredTags
    ) {
        // Make sure we're dealing with ints. The client uses
        // multipart/form-data for some requests and there the tags may be
        // given as strings.
        $desiredTags = array_map('intval', $desiredTags);

        // Assignable are all tags that the user may assign and that belong to
        // one of the tenants the organisation belongs to.
        $assignableTags = ContactTag::forUser($user)
            ->whereHas('tenants', function ($query) use ($organisation) {
                $query->whereIn('tenant.tenant_id', $organisation->tenants()->pluck('tenant.tenant_id'));
            })->get();

        // The assigned tags are the tags already assigned to the contact
        $assignedTags = $contact->tags()->get();

        // The tags the contact already has and that the user may not assign
        // must be kept as the user may not modify those.
        $keepTags = $assignedTags->diff($assignableTags);

        // The new set of tags is the union of the kept tags and the
        // intersection of the desired tags and the assignable tags. $keepTags
        // and $assignableTags are disjoint so we can just concat them
        $newTags = $keepTags->concat($assignableTags->only($desiredTags));

        $contact->tags()->sync($newTags);
    }

    /**
     * Remove a contact and its PDF.
     */
    public function destroy(Organisation $organisation, Contact $contact)
    {
        $this->logRequest();

        if (! $this->userIsAuthorizedToContact($contact)) {
            abort(405, 'Unauthorized request to delete.');
        }

        $contact->delete();
    }

    /**
     * Checks if the extension for a given filename or path equals "pdf".
     *
     * @param $filename
     * @return bool
     */
    protected function hasPdfFileExtension(string $filename): bool
    {
        if (pathinfo($filename, PATHINFO_EXTENSION) === 'pdf') {
            return true;
        }

        return false;
    }

    /**
     * Returns a randomized pdf filename due to the following schema:
     * "<uuid-lv4>.pdf"
     *
     * In case a prefix as passed to the function it will be prepended as follows:
     * "<prefix>_<uuid-lv4>.pdf"
     *
     * @param string|null $prefix
     * @return string
     */
    protected function getRandomizedPfdFilename(string $prefix = null): string
    {
        $filename = Str::uuid() . ".pdf";

        if (isset($prefix)) {
            return $prefix . "_" . $filename;
        }

        return $filename;
    }

    /**
     * Determines if the user is authorized to access or manipulate the given contact.
     *
     * @param Contact $contact
     * @return bool
     */
    protected function userIsAuthorizedToContact(Contact $contact): bool
    {
        $restrictedRoles = (array) json_decode($contact->restrict_to_client_roles);

        if (empty($restrictedRoles)) {
            return true;
        }

        foreach ($restrictedRoles as $restrictedRole) {
            if (in_array($restrictedRole, Auth::user()->getGroups())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Applies the client role restriction conditions to contact query.
     *
     * @param Builder $query
     * @param array $clientRoles
     *
     * @return void
     */
    private function applyClientRoleRestrictionToContactQuery(Builder &$query, array $clientRoles): void
    {
        $query->where(function ($query) {
            $query->whereJsonLength('restrict_to_client_roles', 0)->orWhere(function ($query) {
                $clientRoles = Auth::user()->getGroups();
                $query->whereJsonContains('restrict_to_client_roles', $clientRoles[0]);

                for ($i = 1; $i <= count($clientRoles) - 1; $i++) {
                    $query->orWhereJsonContains('restrict_to_client_roles', $clientRoles[$i]);
                }
            });
        });
    }

    /**
     * Determines if the provided email mail domain is valid for the organisation settings.
     * True, if the organisation has no mail domain assigned or the domain of the email
     * matches the organisations list of mail domains.
     *
     *
     * @param string $email
     * @param int $organisationId
     * @return bool
     */
    private function emailMatchesOrganisation(string $email, int $organisationId): bool
    {
        $mailDomain = explode('@', $email)[1];
        $mailDomains = [];

        foreach (Organisation::getMailDomainsHierarchical($organisationId) as $organisation) {
            foreach ($organisation as $domainList => $domain) {
                $mailDomains[] = $domain;
            }
        };

        if (empty($mailDomains) || in_array($mailDomain, $mailDomains)) {
            return true;
        }

        return false;
    }

    /**
     * Returns the configured tenant roles by organisation.
     *
     * @param Organisation $organisation
     * @return array
     */
    private function getAllowedRolesByOrganisation(Organisation $organisation): array
    {
        $tenantRoles = $this->configService->getGlobalContactRoleMap();

        foreach ($organisation->tenants()->pluck('name')->toArray() as $tenantName) {
            $tenantRoles = array_merge($tenantRoles, $this->configService->getTenantContactRoleMap($tenantName));
        }

        return $tenantRoles;
    }
}
