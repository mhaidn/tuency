<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <magnus.schieder@intevation.de>
 */

namespace App\Http\Controllers;

use App\Models\Organisation;
use App\Models\OrganisationTag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class OrganisationTagController extends Controller
{
    public function index()
    {
        $this->logRequest();
        return OrganisationTag::forUser(Auth::user())->with('tenants')->get();
    }

    /**
     * Create a new organisation tag.
     */
    public function store(Request $request)
    {
        Gate::authorize('manage-organisation-tags');

        $validated = $request->validate([
            'name' => 'string|required',
            'visible' => 'boolean|required',
            'tenants.*' => ['integer', Rule::exists('tenant', 'tenant_id')],
        ]);
        $this->logRequest($validated);

        return DB::transaction(function () use ($validated) {
            $orgaTag = OrganisationTag::create($validated);
            if (array_key_exists('tenants', $validated)) {
                $orgaTag->tenants()->sync($validated['tenants']);
            }
            return $orgaTag;
        });
    }

    /**
     * Update an organisation tag.
     */
    public function update(Request $request, OrganisationTag $organisationTag)
    {
        Gate::authorize('manage-organisation-tags');

        $validated = $request->validate([
            'name' => 'string',
            'visible' => 'boolean',
            'tenants.*' => ['integer', Rule::exists('tenant', 'tenant_id')],
        ]);
        $this->logRequest($validated);

        return DB::transaction(function () use ($organisationTag, $validated) {
            $organisationTag->update($validated);
            if (array_key_exists('tenants', $validated)) {
                $organisationTag->tenants()->sync($validated['tenants']);

                // The set of tenants has changed, so some contacts may not be
                // allowed to have the tag anymore.
                $affectedOrgas = Organisation::forTag($organisationTag->getKey())->with('tags')->get();
                foreach ($affectedOrgas as $orga) {
                    $orga->removeIllegalTags();
                }
            }
            return $organisationTag;
        });
    }

    /**
     * Remove an contacttag
     */
    public function destroy(OrganisationTag $organisationTag)
    {
        Gate::authorize('manage-organisation-tags');

        $this->logRequest();
        $organisationTag->delete();
    }
}
