<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditTrait;

class ClassificationType extends Model implements Auditable
{
    use AuditTrait;

    protected $table = 'classification_type';

    protected $primaryKey = 'classification_type_id';

    // To keep things simple at the start, we omit the time stamps.
    public $timestamps = false;

    protected $fillable = ['name'];

    protected $visible = [
        'classification_type_id',
        'name'
    ];
}
