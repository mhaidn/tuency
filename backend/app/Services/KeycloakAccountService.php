<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Psr\Http\Message\ResponseInterface;

class KeycloakAccountService
{
    /**
     * @var string Keycloak base url.
     */
    private string $keycloakUrl;

    /**
     * @var string Keycloak realm.
     */
    private string $realm;

    /**
     * @var string Keycloak client id.
     */
    private string $clientId;

    /**
     * @var string Keycloak management client secret.
     */
    private string $clientSecret;

    /**
     * @var Client Guzzle HTTP Client.
     */
    private Client $httpClient;

    /**
     * Reads in the account service configuration and instances the http client.
     */
    public function __construct()
    {
        // Retrieve config.
        $this->keycloakUrl = config('keycloak-account-service.url');
        $this->realm = config('keycloak-account-service.realm');
        $this->clientId = config('keycloak-account-service.client_id');
        $this->clientSecret = config('keycloak-account-service.client_secret');

        //Instance guzzle client
        $this->httpClient = new Client(['base_uri' => $this->keycloakUrl]);
    }

    /**
     * Returns the configured keycloak realm.
     *
     * @return string
     */
    public function getRealm(): string
    {
        return $this->realm;
    }

    /**
     * Returns the configured keycloak client id.
     *
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /** ACCOUNT INTERACTION */

    /**
     * Returns a specific user by keycloak id.
     *
     * @param $kcUserId
     * @return mixed
     */
    public function getUser($kcUserId): mixed
    {
        $path = "/users/" . $kcUserId;

        return $this->kcAdminGet($path);
    }

    /**
     * Returns a searchable list of all users.
     * @see(https://www.keycloak.org/docs-api/24.0.1/rest-api/index.html#_users)
     * TODO: Fetch limit / pagination currently globally disabled when no parameters passed.
     *
     * @param null $params
     * @return mixed
     */
    public function getUsers($params = null): mixed
    {
        if (! $params) {
            $params = ['limit' => -1];
        }

        return $this->filterServiceAccounts($this->kcAdminGet("/users", $params));
    }

    /**
     * Filters accounts with name service-account-<client-id>
     * from Keycloaks user list.
     *
     * @param $users
     * @return mixed
     */
    protected function filterServiceAccounts($users): mixed
    {
        $users = array_filter($users, function ($user) {
            return $user->username !== 'service-account-' . $this->clientId;
        });

        return $users;
    }

    /**
     * Creates a new user in keycloak.
     *
     * @param string $username
     * @param string $email
     * @param array $requiredActions
     * @param bool $enabled
     *
     * @return mixed
     */
    public function createUser(string $username, string $email, array $requiredActions, bool $enabled = true): mixed
    {
        $params = [
            'username' => $username,
            'email' => $email,
            'requiredActions' => $requiredActions,
            'enabled' => $enabled
        ];

        return $this->kcAdminPost("/users", $params, true);
    }

    /**
     * Removes a keycloak user.
     *
     * @param string $kcUserUUID
     * @return mixed
     */
    public function deleteUser(string $kcUserUUID)
    {
        $path = "/users/{$kcUserUUID}";

        return $this->kcAdminDelete($path);
    }

    /**
     * Triggers keycloak required action mail for a specific user.
     *
     * @param string $kcUserUUID
     * @param string $clientId
     * @return mixed
     */
    public function executeActionMailByUser(string $kcUserUUID, string $clientId)
    {
        $path = "/users/{$kcUserUUID}/execute-actions-email";

        $query = [
            'client_id' => $clientId,
        ];

        return $this->kcAdminPut($path, $query);
    }

    /**
     * Returns all users for a specific role name.
     *
     * @param string $roleName
     * @return mixed
     */
    public function getUsersOfRole(string $roleName)
    {
        $path = "/clients/" . $this->getClientUUID() . "/roles/" . $roleName . "/users";

        return $this->kcAdminGet($path, ['limit' => -1]);
    }

    /**
     * Returns the assigned roles for a keycloak user id.
     *
     * @param string $kcUserUUID
     * @return mixed
     */
    public function getRolesOfUser(string $kcUserUUID)
    {
        $path = "/users/" . $kcUserUUID . "/role-mappings/clients/" . $this->getClientUUID();

        return $this->kcAdminGet($path, ['limit' => -1]);
    }

    /**
     * Returns the client roles of the configured client.
     *
     * @return mixed
     */
    public function getClientRoles()
    {
        $path = "/clients/" . $this->getClientUUID() . "/roles";

        return $this->kcAdminGet($path);
    }

    /**
     * Returns the client role by role name.
     *
     * @param string $roleName
     * @return mixed
     */
    public function getClientRole(string $roleName)
    {
        $path = "/clients/" . $this->getClientUUID() . "/roles/" . $roleName;

        return $this->kcAdminGet($path);
    }

    /**
     * Applies a set of client roles to a keycloak user id.
     *
     * @param string $kcUserUUID
     * @param array $clientRoles
     * @return mixed
     */
    public function addClientRolesToUser(string $kcUserUUID, array $clientRoles)
    {
        $path = "/users/{$kcUserUUID}/role-mappings/clients/{$this->getClientUUID()}";

        return $this->kcAdminPost($path, $clientRoles);
    }

    /**
     * Removes a set of client roles to a keycloak user id.
     *
     * @param string $kcUserUUID
     * @param array $clientRoles
     * @return mixed
     */
    public function removeClientRolesFromUser(string $kcUserUUID, array $clientRoles)
    {
        $path = "/users/{$kcUserUUID}/role-mappings/clients/{$this->getClientUUID()}";

        return $this->kcAdminDelete($path, $clientRoles);
    }

    /**
     * Loads and caches the client UUID.
     *
     * @return mixed
     */
    public function getClientUUID(): mixed
    {
        return Cache::rememberForever('kc.client-uuid', function () {
            $query = ["clientId" => $this->clientId];
            return $this->kcAdminGet("/clients", $query)[0]->id;
        });
    }

    /** TOKEN HANDLING */

    /**
     * Handles the access token init and refresh process and returns it.
     *
     * @return mixed
     * @throws GuzzleException
     */
    private function getAccessToken(): mixed
    {
        if (Cache::has('kc.access_token')) {
            return Cache::get('kc.access_token');
        }

        $kcTokenResponse = $this->requestClientCredentialGrantToken();

        Cache::put(
            'kc.access_token',
            $kcTokenResponse->access_token,
            now()->addSeconds($kcTokenResponse->expires_in)
        );

        return $kcTokenResponse->access_token;
    }

    /**
     * Performs an openid-connect token request with "client_credentials" grant option
     * to issue a new access token for the configured client_id and client_secret.
     *
     * @return mixed
     * @throws GuzzleException
     */
    private function requestClientCredentialGrantToken(): mixed
    {
        $response = $this->httpClient->post($this->getTokenPath(), [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret
            ]
        ]);

        return $this->extractJsonResponse($response);
    }

    /** HELPERS */

    /**
     * Returns keycloak openid-connect token path.
     *
     * @return string
     */
    private function getTokenPath(): string
    {
        return "/realms/" . $this->realm . "/protocol/openid-connect/token";
    }

    /**
     * Determines if the provided uri contains a path or a full request url.
     * Returns either the detected request url or creates the path for the keycloak admin api.
     *
     * @param string $uri
     * @return string
     */
    private function buildRequestPath(string $uri): string
    {
        if (Str::contains($uri, $this->keycloakUrl)) {
            return $uri;
        }

        return "/admin/realms/" . $this->realm . $uri;
    }

    /**
     * Performs a get request against the keycloak api.
     *
     * @param string $uri
     * @param array|null $queryParams
     * @param bool $extractResponse
     * @return mixed
     */
    private function kcAdminGet(string $uri, array $queryParams = null, bool $extractResponse = true): mixed
    {
        $response = null;

        try {
            $response = $this->httpClient->get(
                $this->buildRequestPath($uri),
                $this->buildGuzzleOptions($queryParams)
            );
        } catch (GuzzleException $e) {
            if ($e->getCode() === 401) {
                Log::error('Error in keycloak token handling. Destroying token cache.');
                $this->destroyTokenCache();
            } else {
                Log::error('Unable to fetch results from keycloak. ' . $e->getMessage());
            }

            exit(500);
        }

        if ($extractResponse) {
            return $this->extractJsonResponse($response);
        }

        return $response;
    }

    /**
     * Performs a post request against the keycloak api.
     * The created resource may be automatically fetched and returned by setting $retrieveResource = true.
     *
     * @param string $uri
     * @param array|null $jsonBody
     * @param bool $retrieveResource
     *
     * @return mixed
     */
    private function kcAdminPost(string $uri, array $jsonBody = null, bool $retrieveResource = false): mixed
    {
        try {
            $response = $this->httpClient->post(
                $this->buildRequestPath($uri),
                $this->buildGuzzleOptions(null, $jsonBody)
            );
        } catch (GuzzleException $e) {
            if ($e->getCode() === 409) {
                abort(409, "Conflicting resource. The resource might already exist.");
            }

            Log::error('Unable to execute post request. ' . $e->getMessage());
            return false;
        }

        if ($retrieveResource) {
            return $this->kcAdminGet($response->getHeader('Location')[0]);
        }

        return true;
    }

    /**
     * Performs a put request against the keycloak api.
     *
     * @param string $uri
     * @param array|null $queryParams
     * @param array|null $jsonBody
     *
     * @return mixed
     */
    private function kcAdminPut(string $uri, array $queryParams = null, array $jsonBody = null): mixed
    {
        try {
            $this->httpClient->put(
                $this->buildRequestPath($uri),
                $this->buildGuzzleOptions($queryParams, $jsonBody)
            );
        } catch (GuzzleException $e) {
            Log::error('Unable to execute PUT request. ' . $e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Performs a delete request against the keycloak api.
     *
     * @param string $uri
     * @param array|null $queryParams
     * @param array|null $jsonBody
     *
     * @return mixed
     */
    private function kcAdminDelete(string $uri, array $queryParams = null, array $jsonBody = null): mixed
    {
        try {
            $this->httpClient->delete(
                $this->buildRequestPath($uri),
                $this->buildGuzzleOptions($queryParams, $jsonBody)
            );
        } catch (GuzzleException $e) {
            Log::error('Unable to execute DELETE request. ' . $e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Assembles the guzzle http options.
     *
     * @param null $queryParams
     * @param null $jsonBody
     * @return array[]
     * @throws GuzzleException
     */
    private function buildGuzzleOptions($queryParams = null, $jsonBody = null): array
    {
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->getAccessToken(),
                'Content-Type' => 'application/json'
            ]
        ];

        if ($queryParams) {
            $options['query'] = $queryParams;
        }

        if ($jsonBody) {
            $options['json'] = $jsonBody;
        }

        return $options;
    }

    private function destroyTokenCache(): void
    {
        Cache::forget('kc.access_token');
    }

    /**
     * Returns the extracted json content as object.
     *
     * @param ResponseInterface $response
     *
     * @return mixed
     */
    private function extractJsonResponse(ResponseInterface $response): mixed
    {
        return json_decode($response->getBody()->getContents());
    }
}
