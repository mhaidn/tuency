<?php

namespace App\Services;

use App\Models\Contact;
use Carbon\Carbon;

/**
 * Supplies
 */
class ContactService
{
    private TuencyConfigService $configService;

    public function __construct()
    {
        $this->configService = new TuencyConfigService();
    }

    /**
     * Returns a set of expired contacts that have not yet been notified
     * or the due date of the notification interval has been exceeded.
     *
     * @param array $columns specific columns to select.
     * @param array $includeClientRoleRestrictions include contacts that are restricted to client roles.
     * @return mixed
     */
    public function getUnnotifiedExpiredContacts(array $columns = [], array $includeClientRoleRestrictions = []): mixed
    {
        return $this->getExpiredContacts(true, $columns, $includeClientRoleRestrictions);
    }

    /**
     * Returns the set of expired contacts.
     *
     * @param bool $onlyNotifiable excludes contacts where expiry notification interval ist exceeded.
     * @param array $columns specific columns to select.
     * @param array $includeClientRoleRestrictions include contacts that are restricted to client roles.
     *
     * @return mixed
     */
    public function getExpiredContacts(
        bool $onlyNotifiable = false,
        array $columns = [],
        array $includeClientRoleRestrictions = []
    ): mixed {
        $expiryDate = $this->configService->getCurrentContactExpiryDate();
        $resendAfterDate = $this->configService->getCurrentContactExpiryNotificationDate();

        // Query contacts
        $expiredContacts = Contact::whereDate('updated_at', '<=', $expiryDate);

        // Conditional fetch of only notifiable ones. (latest expiry warning has not passed the configured delay)
        if ($onlyNotifiable) {
            $expiredContacts->where(function ($query) use ($resendAfterDate) {
                $query->whereDate('latest_expiry_warning', '<=', $resendAfterDate)
                    ->orWhereNull('latest_expiry_warning');
            });
        }

        // Contacts restricted to client roles
        $expiredContacts->where(function ($query) use ($includeClientRoleRestrictions) {

            // Select non restricted roles
            $query->whereJsonLength('restrict_to_client_roles', 0);

            // Apply included roles from filter if present.
            if (! empty($includeClientRoleRestrictions)) {
                $query->orWhere(function ($query) use ($includeClientRoleRestrictions) {
                    $query->whereJsonContains('restrict_to_client_roles', $includeClientRoleRestrictions[0]);

                    for ($i = 1; $i <= count($includeClientRoleRestrictions) - 1; $i++) {
                        $query->orWhereJsonContains('restrict_to_client_roles', $includeClientRoleRestrictions[$i]);
                    }
                });
            }
        });

        return $columns ? $expiredContacts->get($columns) : $expiredContacts->get();
    }
}
