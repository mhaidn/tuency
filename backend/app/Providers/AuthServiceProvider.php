<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        Auth::provider('keycloak', function ($app, array $config) {
            return new KeycloakUserProvider($this->app['hash'], $config['model']);
        });

        Gate::define('list-portals', function ($user) {
            return $user->isPortalAdmin();
        });

        Gate::define('create-portaladmin', function ($user) {
            return $user->isPortalAdmin();
        });

        Gate::define('create-tenantadmin', function ($user) {
            return $user->isPortalAdmin()
                || $user->isTenantAdmin();
        });

        Gate::define('create-orgaadmin', function ($user) {
            return $user->isPortalAdmin()
                || $user->isTenantAdmin()
                || $user->isOrgaAdmin();
        });

        Gate::define('manage-claims', function ($user) {
            return $user->isPortalAdmin()
                || $user->isTenantAdmin();
        });

        Gate::define('tenant-as-parent', function ($user) {
            return $user->isPortalAdmin()
                || $user->isTenantAdmin();
        });

        Gate::define('orga-as-parent', function ($user) {
            return $user->isPortalAdmin()
                || $user->isTenantAdmin()
                || $user->isOrgaAdmin();
        });

        Gate::define('manage-contact-tags', function ($user) {
            return $user->isPortalAdmin();
        });

        Gate::define('assign-contact-tags', function ($user) {
            return $user->isPortalAdmin()
                || $user->isTenantAdmin();
        });

        Gate::define('manage-organisation-tags', function ($user) {
            return $user->isPortalAdmin();
        });

        Gate::define('assign-organisation-tags', function ($user) {
            return $user->isPortalAdmin()
                || $user->isTenantAdmin();
        });

        Gate::define('manage-global-rules', function ($user) {
            return $user->isPortalAdmin();
        });

        Gate::define('show-user-information', function ($user) {
            return $user->isPortalAdmin();
        });
    }
}
