<?php

namespace Database\Factories;

use App\Models\ClassificationTaxonomy;
use App\Models\FeedProvider;
use App\Models\NetworkRule;
use Illuminate\Database\Eloquent\Factories\Factory;

class NetworkRuleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = NetworkRule::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */
    private $countRules = 0;

    public function definition()
    {
        $numNetworkRules = config('tuency.seeders.organisations.networks.rules.count');

        $classifications = ClassificationTaxonomy::with('types')->get()->toArray();
        $feedProviders = FeedProvider::with('feeds')->get()->toArray();

        $newUnique = false;
        if ($this->countRules == 0) {
            $newUnique = true;
        }
        $this->countRules++;

        $randomTaxonomy = $this->faker->randomElement($classifications);
        $randomProvider = $this->faker->randomElement($feedProviders);

        $feedStatusOptions = config('tuency.netobjects.rules.feed.status.enabled') ?
            config('tuency.netobjects.rules.feed.status.options') :
            array(config('tuency.netobjects.rules.feed.status.default'));

        return [
            'classification_taxonomy_id' => $randomTaxonomy['classification_taxonomy_id'],
            'classification_type_id' => $this->faker->unique($reset = $newUnique)
                ->randomElement($randomTaxonomy['types'])['classification_type_id'],
            'feed_provider_id' => $randomProvider['feed_provider_id'],
            'feed_name_id' => $this->faker->randomElement($randomProvider['feeds'])['feed_name_id'],
            'feed_status' => $this->faker->randomElement($feedStatusOptions),
            'suppress' => $this->faker->boolean(),
            'interval_length' => 1,
            'interval_unit' => 'immediate',
            'abuse_c' => false,
        ];
    }
}
