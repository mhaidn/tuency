<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('organisation', function (Blueprint $table) {
            $table->foreignId('parent_id')
                  ->nullable()
                  ->constrained('organisation', 'organisation_id');
            $table->index('parent_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('organisation', function (Blueprint $table) {
            $table->dropColumn('parent_id');
        });
    }
};
