<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('contact', function (Blueprint $table) {
            $table->renameColumn('uri', 'endpoint');
            $table->dropColumn('role');
            $table->json('roles')->default('[]');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('contact', function (Blueprint $table) {
            $table->renameColumn('endpoint', 'uri');
            $table->dropColumn('roles');
            $table->text('role')->default('');
        });
        //
    }
};
