<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("
        UPDATE fqdn SET fqdn = lower(fqdn);
        ");
        DB::statement("
        ALTER TABLE fqdn
          ADD CONSTRAINT fqdn_fqdn_lower_check CHECK (lower(fqdn) = fqdn);
        ");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement("
        ALTER TABLE fqdn
         DROP CONSTRAINT fqdn_fqdn_lower_check;
        ");
    }
};
