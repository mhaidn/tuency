<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    private static $ruleKinds = [
        'organisation',
        'asn',
    ];

    public function ruleTableName(string $kind)
    {
        return "{$kind}_rule";
    }

    public function nmContactTableName(string $kind)
    {
        $ruleTable = $this->ruleTableName($kind);

        if ($ruleTable < 'contact') {
            $nmTable = "{$ruleTable}_contact";
        } else {
            $nmTable = "contact_{$ruleTable}";
        }

        return $nmTable;
    }


    /**
     * Create a rule table.
     *
     * The kind parameter specifies which other kind of data the rule is
     * associated with. E.g. 'organisation' means the rule is associated with
     * an organisation. The kind parameter is incorporated into the name of
     * the rule table (e.g. organisation_rule) and is used for a corresponding
     * foreign key constraint.
     */
    public function createTableFor(string $kind)
    {
        $ruleTable = $this->ruleTableName($kind);

        Schema::create(
            $ruleTable,
            function (Blueprint $table) use ($ruleTable, $kind) {
                $kindId = "{$kind}_id";

                $table->id("{$ruleTable}_id");
                $table->foreignId($kindId)->constrained($kind, $kindId);
                $table->foreignId('classification_taxonomy_id')
                      ->nullable()
                      ->constrained('classification_taxonomy', 'classification_taxonomy_id');
                $table->foreignId('classification_type_id')
                      ->nullable()
                      ->constrained('classification_type', 'classification_type_id');
                $table->foreignId('feed_provider_id')
                      ->nullable()
                      ->constrained('feed_provider', 'feed_provider_id');
                $table->foreignId('feed_name_id')
                      ->nullable()
                      ->constrained('feed_name', 'feed_name_id');
                $table->enum('feed_status', ['production', 'beta']);
                $table->unique([
                    $kindId,
                    'classification_taxonomy_id',
                    'classification_type_id',
                    'feed_provider_id',
                    'feed_name_id',
                    'feed_status',
                ]);
            }
        );

        Schema::create($this->nmContactTableName($kind), function (Blueprint $table) use ($ruleTable) {
            $ruleId = "{$ruleTable}_id";

            $table->foreignId($ruleId)->constrained($ruleTable, $ruleId);
            $table->index($ruleId);
            $table->foreignId('contact_id')
                ->constrained('contact', 'contact_id');

            $table->unique([$ruleId, 'contact_id']);
        });
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        foreach (static::$ruleKinds as $kind) {
            $this->createTableFor($kind);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        foreach (static::$ruleKinds as $kind) {
            Schema::dropIfExists($this->nmContactTableName($kind));
            Schema::dropIfExists($this->ruleTableName($kind));
        }
    }
};
