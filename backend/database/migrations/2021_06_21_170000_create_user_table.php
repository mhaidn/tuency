<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user', function (Blueprint $table) {
            $table->uuid('keycloak_user_id');
            $table->primary('keycloak_user_id');
            $table->timestamps();
        });

        DB::statement("
        INSERT INTO \"user\" (keycloak_user_id)
             SELECT keycloak_user_id FROM tenant_user
              UNION DISTINCT
             SELECT keycloak_user_id FROM organisation_user;
        ");

        Schema::table(
            'tenant_user',
            function (Blueprint $table) {
                $table->foreign('keycloak_user_id')
                    ->references('keycloak_user_id')
                    ->on('user');
            }
        );

        Schema::table(
            'organisation_user',
            function (Blueprint $table) {
                $table->foreign('keycloak_user_id')
                    ->references('keycloak_user_id')
                    ->on('user');
            }
        );
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('organisation_user', function (Blueprint $table) {
            $table->dropForeign(['keycloak_user_id']);
        });

        Schema::table('tenant_user', function (Blueprint $table) {
            $table->dropForeign(['keycloak_user_id']);
        });

        Schema::dropIfExists('user');
    }
};
