<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create(
            'global_rule',
            function (Blueprint $table) {
                $table->id("global_rule_id");
                $table->foreignId('classification_taxonomy_id')
                      ->constrained('classification_taxonomy', 'classification_taxonomy_id');
                $table->foreignId('classification_type_id')
                      ->constrained('classification_type', 'classification_type_id');
                $table->foreignId('feed_provider_id')
                      ->constrained('feed_provider', 'feed_provider_id');
                $table->foreignId('feed_name_id')
                      ->constrained('feed_name', 'feed_name_id');
                $table->enum('feed_status', ['production', 'beta', 'any']);
                $table->boolean('suppress');
                $table->integer('interval_length');
                $table->enum('interval_unit', ['immediate', 'hours', 'days', 'weeks', 'month']);
                $table->unique([
                    'classification_taxonomy_id',
                    'classification_type_id',
                    'feed_provider_id',
                    'feed_name_id',
                    'feed_status',
                ]);
            }
        );
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('global_rule');
    }
};
