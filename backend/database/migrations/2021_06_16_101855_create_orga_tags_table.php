<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('organisation_tag', function (Blueprint $table) {
            $table->id('organisation_tag_id');
            $table->text('name')->unique();
            $table->boolean('visible');
            $table->timestamps();
        });

        Schema::create('organisation_organisation_tag', function (Blueprint $table) {
            $table->foreignId('organisation_id')
                ->constrained('organisation', 'organisation_id');
            $table->foreignId('organisation_tag_id')
                ->constrained('organisation_tag', 'organisation_tag_id');
            $table->unique(['organisation_id', 'organisation_tag_id']);
            $table->timestamps();
        });

        Schema::create('organisation_tag_tenant', function (Blueprint $table) {
            $table->foreignId('organisation_tag_id')
                ->constrained('organisation_tag', 'organisation_tag_id');
            $table->foreignId('tenant_id')
                ->constrained('tenant', 'tenant_id');
            $table->unique(['organisation_tag_id', 'tenant_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('organisation_tag_tenant');
        Schema::dropIfExists('organisation_organisation_tag');
        Schema::dropIfExists('organisation_tag');
    }
};
