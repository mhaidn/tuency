<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('pdf', function (Blueprint $table) {
            $table->text('pdf')->change();
            $table->foreignId('contact_id')
                  ->constrained('contact', 'contact_id');
        });
        Schema::table('contact', function (Blueprint $table) {
            $table->dropForeign('contact_pdf_id_foreign');
            $table->dropColumn('pdf_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('pdf', function (Blueprint $table) {
            $table->binary('pdf')->change();
            $table->foreignId('pdf_id')
                ->nullable()
                ->constrained('pdf', 'pdf_id');
        });
    }
};
