<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    private static $tableNames = [
        'organisation_tenant',
        'organisation_user',
        'tenant_user',
    ];

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        foreach (static::$tableNames as $tableName) {
            Schema::table($tableName, function (Blueprint $table) {
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        foreach (static::$tableNames as $tableName) {
            Schema::table($tableName, function (Blueprint $table) {
                $table->dropColumn(['created_at', 'updated_at']);
            });
        }
    }
};
