<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('fqdn', function (Blueprint $table) {
            $table->id('fqdn_id');
            $table->foreignId('organisation_id')
                  ->constrained('organisation', 'organisation_id');
            $table->text('fqdn');
            $table->enum('approval', ['pending', 'approved', 'denied']);

            $table->unique(['organisation_id', 'fqdn']);
            $table->index('fqdn');
            $table->index('approval');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('fqdn');
    }
};
