<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('contact', function (Blueprint $table) {
            $table->text('name')->default('')->change();
            $table->text('email')->default('')->change();
            $table->text('phone')->default('')->change();
            $table->text('pgp')->default('')->change();
            $table->text('commentary')->default('')->change();
            $table->text('uri')->default('')->change();
            $table->integer('zip')->default(-1)->change();
            $table->renameColumn('name', 'last_name');
            $table->renameColumn('phone', 'phone_1');
            $table->renameColumn('pgp', 'openpgp_pubkey');
            $table->renameColumn('s_mime', 's_mime_cert');
        });
        //
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('contact', function (Blueprint $table) {
            $table->renameColumn('last_name', 'name');
            $table->renameColumn('phone_1', 'phone');
            $table->renameColumn('openpgp_pubkey', 'pgp');
            $table->renameColumn('s_mime_cert', 's_mime');
        });
    }
};
