<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('organisation_user', function (Blueprint $table) {
            $table->foreignId('organisation_id')
                ->constrained('organisation', 'organisation_id');
            $table->index('organisation_id');
            $table->uuid('keycloak_user_id');
            $table->index('keycloak_user_id');

            $table->unique(['organisation_id', 'keycloak_user_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('organisation_user');
    }
};
