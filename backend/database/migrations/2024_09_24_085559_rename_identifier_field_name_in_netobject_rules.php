<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('asn_rule', function (Blueprint $table) {
            $table->renameColumn('identifier', 'classification_identifier');
        });

        Schema::table('fqdn_rule', function (Blueprint $table) {
            $table->renameColumn('identifier', 'classification_identifier');
        });

        Schema::table('network_rule', function (Blueprint $table) {
            $table->renameColumn('identifier', 'classification_identifier');
        });

        Schema::table('organisation_rule', function (Blueprint $table) {
            $table->renameColumn('identifier', 'classification_identifier');
        });

        Schema::table('global_rule', function (Blueprint $table) {
            $table->renameColumn('identifier', 'classification_identifier');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('asn_rule', function (Blueprint $table) {
            $table->renameColumn('classification_identifier', 'identifier');
        });

        Schema::table('fqdn_rule', function (Blueprint $table) {
            $table->renameColumn('classification_identifier', 'identifier');
        });

        Schema::table('network_rule', function (Blueprint $table) {
            $table->renameColumn('classification_identifier', 'identifier');
        });

        Schema::table('organisation_rule', function (Blueprint $table) {
            $table->renameColumn('classification_identifier', 'identifier');
        });

        Schema::table('global_rule', function (Blueprint $table) {
            $table->renameColumn('classification_identifier', 'identifier');
        });
    }
};
