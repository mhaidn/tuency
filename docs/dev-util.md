# The "dev-util" script

The bash utility placed in ``./docker/dev-util`` was created to easily set up the project and manage the frontend and backend
toolchain during development. Besides the helper function  it also offers as set of cli tools to work with the backend and client framework.  
The main approach is that a developer has all framework dependencies (PHP, Node, NPM) shipped and managed over the
docker setup and does not have to deal about local versions or how the commands are executed within the container.

## Docker Compose project 

### Setup

The project can be started using the following command:

````bash
# Setup development
./docker/dev-util setup
````

This triggers the following steps:

- Prepare the docker-compose setup, backend and client app
  - Copy docker-compose.dev.yml and .env.example to the docker-compose default file names.
  - Copy the .env.example for backend and client to .env in the respective folder
  - Start the project using ``docker compose up -d`` and build the backend and client container on the first run
  - Make sure the mounted app sources have the appropriate permission and owner settings within the container
- Setup backend and keycloak database
  - The SQL create scripts are mounted into postgres docker-entrypoint and are executed automatically when the container starts for the first time.
- Setup the laravel backend
  - Run a composer (vendor) install
  - Run the database migrations and seeders
  - Clear potential configs and caches
- Setup the client
  - Run a yarn (vendor) install
  - Build the frontend
- Setup Keycloak
  - Create and setup "tuency" realm and management user
  - Setup three clients, roles and theming
  - Copy the "tuency" realms signing public key to the backend environment file.

This command can also be used to re-setup the entire application stack.  
The script will automatically detect an existing installation and will ask for confirmation to drop the
existing data before executing.

### Rebuild containers
When making changes to the backends or clients Dockerfile the local containers must be rebuilt.
This can be done using the following command.

````bash
# Rebuild backend and client container
./docker/dev-util build <backend|client>
````

## Client and backend framework

To interact with the client and backend framework the following commands are available:

- artisan - *Laravel console*
- composer - *PHP package manager*
- npm - *Node package manager*
- yarn - *Extended node resource negotiator (Recommended to use)*

Whenever one of these commands is called the utility script will automatically execute it including additional parameters within the respective container.
This ensures that a developer has all dependencies (PHP, FPM, Composer, Node, NPM...) shipped and managed over the docker setup and does not have to deal about their versions and how the commands are executed within the container.

### Client examples
The most common action needed when working on the client might be rebuilding the vue application on file changes:

````bash
# Watch and rebuild on file changes
./docker/dev-util yarn build --watch
````

Another use case might be adding a new npm dependency to the client app:

````bash
# Apply a new node dependency
./docker/dev-util yarn add <package>
````

### Backend examples
Working with the composer package manager:
````bash
# Install the current vendor state from composer.lock
./docker/dev-util composer install

# Update composer packages according to the version definition in composer.json
./docker/dev-util composer update
````

Working with [Laravels Artisan Console](https://laravel.com/docs/8.x/artisan) which is a powerful utility
that allows to create framework resources (models, providers, middlewares) and manage components such as
config, cache, migrations, seeders.
````bash
# Using the artisan console
./docker/dev-util artisan <command>

# Or the short form
./docker/dev-util art <command>
````

## Helpers

### RIPE Importer
As explained in more detail in the [RIPE Data](../backend/README.md#ripe-data) section of the backend readme, Tuency can work with data imported from RIPE.  
With the following command, the data records for the country code AT can be automatically imported into the Tuency database.  
If you want to import data records for other country codes, adapt the variable `RIPE_IMPORTER_RESTRICT_TO_COUNTRY` in the `./docker/dev-util` script.
  
**Please note:** The import process is very RAM-intensive and can take several minutes.

```bash
# Import RIPE data for default country code "AT"
/docker/dev-util ripe-import
```