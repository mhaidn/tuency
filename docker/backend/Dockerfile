#######################################################################################################################
# PHP-FPM
#######################################################################################################################
FROM php:8.2.20-fpm-bookworm AS php-fpm

ARG USER_ID=1000
RUN usermod -u $USER_ID www-data

############################################################
# Basic setup and configuration
############################################################

# Required host packages
RUN apt-get update && \
    apt-get install -y \
    zip \
    unzip \
    libicu-dev \
    libpq-dev

# docker-php-ext installs
RUN docker-php-ext-install \
    opcache \
    pdo \
    pdo_pgsql \
    bcmath

# Adding default configurations
COPY ./docker/backend/php/opcache.ini /usr/local/etc/php/conf.d/20-opcache.ini
COPY ./docker/backend/php/php-ini-overrides.ini /usr/local/etc/php/conf.d/99-overrides.ini
COPY ./docker/backend/php-fpm/symfony.pool.conf /usr/local/etc/php-fpm.d/symfony.pool.conf
COPY ./docker/backend/php-fpm/www.conf /usr/local/etc/php-fpm.d/www.conf
COPY ./docker/backend/cron/entrypoint-scheduler.sh /opt/entrypoint-scheduler.sh

#######################################################################################################################
# NGINX
#######################################################################################################################
FROM php-fpm AS php-fpm-nginx

WORKDIR /var/www

USER root

############################################################
# Basic setup and configuration
############################################################

# Install and configure nginx
RUN apt --allow-releaseinfo-change update && apt install -y nginx-light
RUN mkdir public && echo "<?php phpinfo(); die();" > public/index.php && chown -R www-data:www-data /var/www

COPY ./docker/backend/nginx/laravel.default /etc/nginx/sites-available/php-fpm-nginx.default
RUN ln -sfn /etc/nginx/sites-available/php-fpm-nginx.default /etc/nginx/sites-enabled/default

#######################################################################################################################
# Docker
#######################################################################################################################

############################################################
# Container config
############################################################

# Expose port
EXPOSE 80

# Copy and set entrypoint
COPY ./docker/backend/docker-entrypoint.sh /bin/docker-entrypoint.sh
ENTRYPOINT ["/bin/docker-entrypoint.sh"]

# Set default command
CMD bash -c "service nginx restart && php-fpm"

############################################################
# Labels
############################################################
LABEL org.opencontainers.image.title="tuency-php-fpm-nginx" \
    org.opencontainers.image.description="Combined image of php, php-fpm and nginx to serve the tuency backend." \
    org.opencontainers.image.base.name="php:8.2-fpm-bookworm" \
    org.opencontainers.image.vendor="cert.at GmbH" \
    org.opencontainers.image.authors="martin@haidn.eu" \
    org.opencontainers.image.licenses="AGPL-3.0-or-later"

#######################################################################################################################
# Dev dependencies
#######################################################################################################################
FROM php-fpm-nginx AS dev

# git
RUN apt update && \
    apt install -y git

# xdebug
RUN pecl install xdebug && \
    docker-php-ext-enable xdebug

# Composer
RUN EXPECTED_CHECKSUM=`curl -XGET https://composer.github.io/installer.sig` \
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && ACTUAL_CHECKSUM=`php -r "echo hash_file('sha384', 'composer-setup.php');"` \
    && if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]; \
        then \
            >&2 echo 'ERROR: Invalid installer checksum'; \
            rm composer-setup.php; \
            exit 1; \
        fi \
    && php composer-setup.php --install-dir=/usr/bin --filename=composer --quiet \
    && RESULT=$? \
    && if [ "$RESULT" != 0 ]; then exit $RESULT; fi \
    && rm composer-setup.php

#######################################################################################################################
# Prod build (Expects composer vendors to be installed)
#######################################################################################################################
FROM php-fpm-nginx AS prod

# Adding prod configuration
COPY ./docker/backend/php/php-ini-overrides-prod.ini /usr/local/etc/php/conf.d/99-overrides.ini

# Moving backend source to webroot.
COPY /backend /var/www

# Adapting file ownership and permissions.
RUN chown -R www-data:www-data /var/www && \
    chmod -R 540 /var/www && \
    chmod -R 740 /var/www/storage /var/www/bootstrap/cache

