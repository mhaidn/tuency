// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
Cypress.Commands.add('login', (username, password) => {
    cy.visit(Cypress.env('clientUrl'));
    cy.get('#username').type(username);
    cy.get('#password').type(password);
    cy.get('#kc-login').click();
    cy.get('.v-main').contains('Manage Netobjects');
    cy.get('#netobjectId table').should('be.visible');
    cy.get('table .v-progress-linear--visible').should('not.exist');
    cy.log('Login successful: ' + username);
});

Cypress.Commands.add('logout', () => {
    cy.get('.v-app-bar--fixed button:last-child').click();
    cy.get('.menuable__content__active div:last-child').contains('Log out').click();
    cy.get('#kc-page-title').contains('Sign in to your account');
    cy.log('Logout successful');
});

Cypress.Commands.add('clickNavItem', (navItemText, assertText) => {
    cy.get('.v-navigation-drawer').within(($navigation) => {
        cy.contains('.v-list-item', navItemText).click();
    });

    cy.get('.v-main').contains(assertText ?? navItemText).should('not.contain.text', 'Loading items...');
    cy.get('.v-main .v-progress-linear__indeterminate--active').should('not.exist');
});

Cypress.Commands.add('clickTableRowActionIcon', (searchString, iconClass) => {
    cy.get('table tbody').contains(searchString).parent().within(($tr) => {
        cy.get(iconClass).click();
    });
});

Cypress.Commands.add('inviteUser', (email, role, tenant) => {
    cy.clickNavItem('My node', 'My Node');
    cy.get('table .v-progress-linear--visible').should('not.exist');
    cy.get('#home button').contains('New User').click();

    cy.get('.v-card form').within(($addUserForm) => {
        cy.get('input:nth-child(2)').type(email);
        cy.get('input:nth-child(1)').click();
    });

    let roleIcon = '';
    // Select tenant by provided name.
    cy.get('[role="listbox"]').contains(tenant).click();

    if (role === 'tenantadmin') {
        roleIcon = 'mdi-alpha-t-box-outline';
    }

    if (role === 'orgaadmin') {
        roleIcon = 'mdi-alpha-o-box-outline';

        cy.get('.v-dialog--active').then(($addUserDialog) => {

            /*
             * Since the table for the organisation select is only displayed if the backend query contains
             * ten or fewer entries, a primitive text filter (typing letter "a") is applied to reduce the result set
             * if necessary. The database seeder creates ten random organisations per default which is why depending
             * on previous tests it might happen, that the organisations must be filtered before one can be selected.
             * This is a quite dull work around for the moment. In the future, the invitation for a specific
             * organisation should be done by name.
             */
            if ($addUserDialog.text().includes('Search For an Organisation')) {
                cy.get('.col-12:nth-child(2) input').type('a');
            }

            cy.get('.v-dialog--active tbody').should('be.visible');
            cy.get('.v-dialog--active tbody tr:nth-child(1)').click();
        });
    }

    cy.get('.v-dialog .v-btn:last-child').contains('Invite').click();

    cy.get('table tbody').contains(email).parent().get('td i').should('have.class', roleIcon);
});

Cypress.Commands.add('createTag', (type, name, tenants, visibleToOrgaadmin = true) => {
    cy.clickNavItem('Tags', 'Manage Tags');

    cy.get('#tagsId').within(($tagsOverviewWrapper) => {
        cy.get('.v-card').get('.v-progress-linear--visible').should('not.exist');

        if (type === 'contact') {
            cy.get('div').contains('Contact Tags').parent().get('button').contains('New Contact Tag').click();
        }

        if (type === 'organisation') {
            cy.get('div').contains('Organisation Tags').parent().parent().get('button').contains('New Organisation Tag').click();
        }
    });

    cy.get('.v-dialog--active').within(($tagCreateModal) => {
        cy.get('.v-input:nth-child(1) input').type(name);

        if (visibleToOrgaadmin) {
            cy.get('.v-input:nth-child(3) input').parent().click();
        }

        cy.get('.v-input:nth-child(2) input:first-child').click();
    });

    cy.get('[role="listbox"]').within(($tenantSelect) => {
        tenants.forEach(tenant => {
            cy.get('[role="option"]').contains(tenant).click();
        });
    });
    cy.get('.v-dialog--active .v-card__title').click(); // Click outside of the tenant multi select.

    cy.get('.v-dialog--active .justify-end button:last-child').contains('Add').click();

    let tenantCount = 1;
    const tagSelector = type === 'contact' ? 'Contact Tags' : type === 'organisation' ? 'Organisation Tags' : 'Unknown tag type.';

    cy.get('div').contains(tagSelector).get('tbody').contains(name).parent().within(($tagRow) => {
        tenants.forEach(tenant => {
            cy.get('td:nth-child(2) .mx-1:nth-child(' + tenantCount + ')').within(($tenantIcon) => {
                cy.get('.v-image__image--preload').should('not.exist');
                cy.get('.v-image__image').should('have.attr', 'style').as('contactTagImageStyle');
                cy.get('@contactTagImageStyle').should(function (style) {
                    expect(style).to.contain(tenant);
                });
            });
            tenantCount++;
        });
    });
});

Cypress.Commands.add('typeOrganisationSearch', (orgaName) => {
    cy.get('.v-label').contains('Search for Organisation(s)').parent().within(($orgaSearch) => {
        cy.get('input').type(orgaName);
    });

    cy.get('table .v-progress-linear--visible').should('not.exist');
});

Cypress.Commands.add('typeContactSearch', (contactName) => {
    cy.get('.v-label').contains('Search for Contact(s)').parent().within(($contactSearch) => {
        cy.get('input').type(contactName);
    });
});

Cypress.Commands.add('clickContactCardAction', (contactName, iconClass) => {
    cy.get('.v-data-iterator header').contains(contactName).parent().within(($contactCardHeader) => {
        cy.get(iconClass).click();
    });
});