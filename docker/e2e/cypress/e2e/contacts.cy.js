let userData;
let contactData;
let contactTag;

before('Load users and contacts.', () => {
    cy.fixture('users').then((users) => {
        userData = users;
    });

    cy.fixture('contacts').then((contacts) => {
        contactData = contacts;
    });
});

afterEach('Logout.', () => {
    cy.logout();
});

describe('Portaladmin in "Tags" tab can', () => {
    beforeEach('Login as portal admin.', () => {
        cy.login(userData.portaladmin.username, userData.portaladmin.password);
    });

    it('creates a contact tag', () => {
        contactTag = 'contact-' + (Math.random() + 1).toString(36).substring(8);
        cy.createTag('contact', contactTag, ['cert.mrk', 'h20cert.mrk']);
    });
});

describe('Portaladmin in "Contacts" tab can', () => {
    beforeEach('Login as portal admin.', () => {
        cy.login(userData.portaladmin.username, userData.portaladmin.password);
    });

    it('create organisation primary contact', () => {
        cy.clickNavItem('Contacts');
        cy.get('.v-data-iterator').should('exist');
        cy.get('.v-btn--bottom .mdi-plus').click();
        cy.get('.v-dialog--active .v-progress-linear--visible').should('not.exist');

        cy.get('.v-dialog--active .row:nth-child(2) .v-input').click();
        cy.get('[role="listbox"]').within(($orgaSelect) => {
            cy.get('.v-list-item:first-child').click();
        });

        cy.get('.v-dialog--active [role="button"]').contains('Roles').parent().click();
        cy.get('.menuable__content__active [role="listbox"]').within(($roleSelect) => {
            cy.get('.v-list-item:first-child').contains('Ansprechpartner Primaer').click();
        });
        cy.get('.v-dialog--active').click(); // Click outside of the tenant multi select.

        cy.get('.v-dialog--active .container:last-child').within(($contactForm) => {

            cy.get('.v-label').contains('First Name').parent().type(contactData.primary.firstName);
            cy.get('.v-label').contains('Last Name').parent().type(contactData.primary.lastName);
            cy.get('.v-label').contains('Email').parent().type(contactData.primary.email);
            cy.get('.v-label').contains('Phone 1').parent().type(contactData.primary.phone);

            cy.fixture('Test-Example.pdf').then(fileContent => {
                cy.get('.v-file-input').contains('AEC Code of Conduct').parent().within(($aecFileUpload) => {
                    cy.get('input').selectFile({
                        contents: Cypress.Buffer.from(fileContent),
                        fileName: 'aec-coc-example.pdf',
                        mimeType: 'application/pdf',
                    }, {
                        force: true // Enforcing file select because the required input is actually not visible.
                    });
                });
            });
        });

        cy.get('.v-dialog--active .v-card__actions button').contains('Create').click();
        cy.typeContactSearch(contactData.primary.firstName);
        cy.get('.v-data-iterator').contains(contactData.primary.email);
    });

    it('ensure that newly created contact is excluded by "Expired only" filter', () => {
        cy.clickNavItem('Contacts');
        cy.typeContactSearch(contactData.primary.firstName);
        cy.get('.v-data-iterator').contains(contactData.primary.email).should('exist');

        cy.get('.v-input').contains('Expired only').parent().click();
        cy.get('.v-main .v-progress-linear--visible').should('not.exist');
        cy.get('.v-data-iterator').contains(contactData.primary.email).should('not.exist');
    });

    it('update organisation primary contact', () => {
        cy.clickNavItem('Contacts');
        cy.typeContactSearch(contactData.primary.firstName);

        cy.clickContactCardAction(contactData.primary.firstName, '.mdi-pencil');

        cy.get('.v-dialog--active [role="button"]').contains('Tags').parent().click();
        cy.get('[role="listbox"] .v-list-item').contains(contactTag).click();
        cy.get('.v-dialog--active').click(); // Click outside of the tenant multi select.

        const commentary = 'This contact was updated. "' + (Math.random() + 1).toString(36).substring(2) + '"';
        cy.get('.v-dialog--active .container .v-label').contains('Commentary').parent().type(commentary);

        cy.get('.v-dialog--active .v-card__actions button').contains('Update').click();
        cy.get('.v-dialog--active').should('not.exist');
        cy.get('.v-input-controll v-progress-linear__indeterminate--active').should('not.exist');

        cy.get('.v-data-iterator header').contains(contactData.primary.firstName).parents('.v-card').within(($contactCard) => {
            cy.get('.v-card__text').contains(commentary);
            cy.get('.v-card__text').contains(contactTag);
        });
    });

    const path = require("path");

    /**
     * NOTE: This test currently only works for chrome since firefox is not
     * able to handle the download window that opens.
     */
    it('download aec-coc file from primary contact', {browser: 'chrome'}, () => {
        cy.clickNavItem('Contacts');
        cy.typeContactSearch(contactData.primary.firstName);

        cy.get('.v-data-iterator header').contains(contactData.primary.firstName).parents('.v-card').within(($contactCard) => {
            cy.get('label').contains('aec_code_of_conduct').parents('.v-input').within(($aecCocFile) => {
                cy.get('label').invoke('text').as('downloadFileName');
                cy.get('.mdi-download').click();
            });
        });

        cy.get('@downloadFileName').then((fileName) => {
            const downloadsFolder = Cypress.config("downloadsFolder");
            cy.readFile(path.join(downloadsFolder, fileName)).should("exist");
        });

        // Note: It is intended to not log out here.
        // The pdf download will open the document in a new tab and returning to the
        // previous application tab just to log out was not yet worth the method.
        // TODO: Consider to use tab switches if the ctrl+<click> organisation actions are tested.
    });

    it('delete organisation primary contact', () => {
        cy.clickNavItem('Contacts');
        cy.typeContactSearch(contactData.primary.firstName);

        cy.clickContactCardAction(contactData.primary.firstName, '.mdi-delete');
        cy.get('.v-dialog--active button').contains('yes').click();

        cy.get('.v-data-iterator').contains(contactData.primary.email).should('not.exist');
    });

    it('create admin contact', () => {
        cy.clickNavItem('Contacts');
        cy.get('.v-data-iterator').should('exist');
        cy.get('.v-btn--bottom .mdi-plus-lock').click();
        cy.get('.v-dialog--active .v-progress-linear--visible').should('not.exist');

        cy.get('.v-dialog--active .row:nth-child(2) .v-input').click();
        cy.get('[role="listbox"]').within(($orgaSelect) => {
            cy.get('.v-list-item:first-child').click();
        });

        cy.get('.v-dialog--active [role="button"]').contains('Roles').parent().click();
        cy.get('.menuable__content__active [role="listbox"]').within(($roleSelect) => {
            cy.get('.v-list-item:first-child').contains('Portal-Admin Contact').click();
        });
        cy.get('.v-dialog--active').click(); // Click outside of the tenant multi select.

        cy.get('.v-dialog--active .container:last-child').within(($contactForm) => {
            cy.get('.v-label').contains('First Name').parent().type(contactData.admin.firstName);
            cy.get('.v-label').contains('Last Name').parent().type(contactData.admin.lastName);
            cy.get('.v-label').contains('Email').parent().type(contactData.admin.email);
            cy.get('.v-label').contains('Phone 1').parent().type(contactData.admin.phone);
        });

        cy.get('.v-dialog--active .v-card__actions button').contains('Create').click();
        cy.typeContactSearch(contactData.admin.firstName);
        cy.get('.v-data-iterator').contains(contactData.admin.email).parents('.v-card').within(($adminContactCard) => {
            cy.get('header i').should('have.class', 'mdi-shield-lock-outline');
        });
    });
});