let userData;
let domain;

before('Load users.', () => {
    cy.fixture('users').then((users) => {
        userData = users;
    });
});

afterEach('Logout.', () => {
    cy.logout();
});

describe('Portaladmin in "Domains" tab can', () => {

    beforeEach('Login as portal admin.', () => {
        cy.login(userData.portaladmin.username, userData.portaladmin.password);
    });
    
    it('create domain netobject that is auto-approved', () => {
        cy.clickNavItem('Domains');
        
        domain = (Math.random() + 1).toString(36).substring(2) + '.at';
        cy.createNewNetobjectForRandomOrga(domain, 'approved');
    });

    it('apply domain netobject for approval again', () => {
        cy.clickNavItem('Domains');
        cy.reapplyForClaimResolution(domain, '.mdi-backup-restore');
    });

    it('approve domain netobject', () => {
        cy.clickNavItem('Claim Resolution');
        cy.resolveNetobject(domain, true);
    });

    it('delete asn netobject', () => {
        cy.clickNavItem('Domains');
        cy.deleteNetobject(domain);
    });
});