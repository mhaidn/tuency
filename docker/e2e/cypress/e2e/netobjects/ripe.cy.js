let userData;
let ripeHandle;

before('Load users.', () => {
    cy.fixture('users').then((users) => {
        userData = users;
    });
});

afterEach('Logout.', () => {
    cy.logout();
});

describe('Portaladmin in "Ripe Handle" tab can', () => {

    beforeEach('Login as portal admin.', () => {
        cy.login(userData.portaladmin.username, userData.portaladmin.password);
    });
    
    it('create RIPE handle netobject that is auto-approved', () => {
        cy.clickNavItem('RIPE Handle');
        
        ripeHandle = 'CY_RIPE_' + Math.floor(Math.random() * (10000 - 1) + 1);
        cy.createNewNetobjectForRandomOrga(ripeHandle, 'approved');
    });

    it('apply RIPE handle netobject for approval again', () => {
        cy.clickNavItem('RIPE Handle');
        cy.typeNetobjectSearch(ripeHandle);

        cy.reapplyForClaimResolution(ripeHandle, '.mdi-backup-restore');
    });

    it('approve RIPE handle netobject', () => {
        cy.clickNavItem('Claim Resolution');
        cy.resolveNetobject(ripeHandle, true);
    });

    it('delete RIPE handle netobject', () => {
        cy.clickNavItem('RIPE Handle');
        cy.typeNetobjectSearch(ripeHandle);

        cy.deleteNetobject(ripeHandle);
    });
});