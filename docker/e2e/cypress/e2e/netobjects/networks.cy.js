let userData;
let ipV4;

before('Load users.', () => {
    cy.fixture('users').then((users) => {
        userData = users;
    });
});

afterEach('Logout.', () => {
    cy.logout();
});

describe('Portaladmin in "Networks" tab can', () => {
    beforeEach('Login as portal admin.', () => {
        cy.login(userData.portaladmin.username, userData.portaladmin.password);
    });
    
    it('create network netobject that is auto-approved', () => {
        cy.clickNavItem('Networks');
        
        ipV4 = '44.123.204.48';
        cy.createNewNetobjectForRandomOrga(ipV4 + '/28', 'approved');
    });

    it('apply network netobject for approval again', () => {
        cy.clickNavItem('Networks');
        cy.typeNetobjectSearch(ipV4);

        cy.reapplyForClaimResolution(ipV4, '.mdi-backup-restore');
    });

    it('approve network netobject', () => {
        cy.clickNavItem('Claim Resolution');

        cy.resolveNetobject(ipV4, true);
    });

    it('delete network netobject', () => {
        cy.clickNavItem('Networks');
        cy.typeNetobjectSearch(ipV4);

        cy.deleteNetobject(ipV4);
    });
});