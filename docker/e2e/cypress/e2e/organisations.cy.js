let userData;
let orgaTag;
let mainOrgaName;

before('Load users.', () => {
    cy.fixture('users').then((users) => {
        userData = users;
    });
});

afterEach('Logout.', () => {
    cy.logout();
});

describe('Portaladmin in "Tags" tab can', () => {
    beforeEach('Login as portal admin.', () => {
        cy.login(userData.portaladmin.username, userData.portaladmin.password);
    });

    it('create an organisation tag', () => {
        orgaTag = 'orga-' + (Math.random() + 1).toString(36).substring(8);
        cy.createTag('organisation', orgaTag, ['cert.mrk', 'h20cert.mrk']);
    });
});

describe('Portaladmin in "Organisations" tab can', () => {
    beforeEach('Login as portal admin.', () => {
        cy.login(userData.portaladmin.username, userData.portaladmin.password);
    });

    it('create untagged main organisation', () => {
        cy.clickNavItem('Organisations');

        mainOrgaName = 'Test Orga-' + Math.floor(Math.random() * (1000 - 1) + 1);

        cy.get('.v-main .justify-end button').contains('New Organisation').click();

        cy.get('.v-dialog--active').within(($orgaCreateDialog) => {
            cy.get('.container').contains('Sub-organisation to:'); // Wait for this field because organisations are fetched in the backgorund.

            cy.get('.v-label').contains('Organisation name').parent().within(($orgaNameField) => {
                cy.get('input').type(mainOrgaName);
            });

            cy.get('.v-label').contains('Tenant(s)').parent().click();
        });

        cy.get('[role="listbox"] .v-list-item:first-child').click();
        cy.get('.v-dialog--active .v-card__title').click(); // Click outside of the tenant multi select.

        cy.get('.v-dialog--active .v-card__actions button:last-child').contains('Add').click();

        cy.typeOrganisationSearch(mainOrgaName);

        cy.get('tbody').contains(mainOrgaName);
    });

    it('update main organisation with tag', () => {
        cy.clickNavItem('Organisations');
        cy.typeOrganisationSearch(mainOrgaName);
        cy.clickTableRowActionIcon(mainOrgaName, '.mdi-pencil');

        cy.get('.v-dialog--active').within(($orgaEditDialog) => {
            cy.get('.v-label').contains('Tag(s)').parent().click();
        });

        cy.get('[role="listbox"] .v-list-item').contains(orgaTag).click();
        cy.get('.v-dialog--active .v-card__title').click(); // Click outside of the tenant multi select.

        cy.get('.v-dialog--active .v-card__actions button:last-child').contains('save').click();

        cy.get('tbody tr').contains(mainOrgaName).parent().contains(orgaTag);
    });

    it('delete a main organisation', () => {
        cy.clickNavItem('Organisations');
        cy.typeOrganisationSearch(mainOrgaName);
        cy.clickTableRowActionIcon(mainOrgaName, '.mdi-delete');

        cy.get('.v-dialog--active button:last-child').contains('yes').click();
        cy.get('table .v-progress-linear--visible').should('not.exist');

        cy.get('table tbody').contains(mainOrgaName).should('not.exist');
    });
});