#######################################################################################################################
# NGINX
#######################################################################################################################
FROM nginx:1.25.2-bookworm as nginx

ARG USER_ID=1000
RUN usermod -u $USER_ID www-data

WORKDIR /var/www/client

############################################################
# Labels
############################################################
LABEL org.opencontainers.image.title="tuency-nginx" \
    org.opencontainers.image.description="Nginx webserver image to serve the tuency client." \
    org.opencontainers.image.base.name="nginx:1.25.2-bookworm" \
    org.opencontainers.image.vendor="cert.at GmbH" \
    org.opencontainers.image.authors="martin@haidn.eu" \
    org.opencontainers.image.licenses="AGPL-3.0-or-later"

#######################################################################################################################
# Dev dependencies
#######################################################################################################################
FROM nginx as dev

# Apply host configs for local tenants.
COPY ./docker/client/nginx/conf.d /etc/nginx/conf.d

# node.js - v18 active LTS
RUN curl https://nodejs.org/dist/v18.18.2/node-v18.18.2-linux-x64.tar.gz --output node-v18.18.2-linux-x64.tar.gz && \
    tar -xf node-v18.18.2-linux-x64.tar.gz && \
    rm node-v18.18.2-linux-x64.tar.gz && \
    cp -p node-v18.18.2-linux-x64/bin/node /usr/local/bin/ && \
    update-alternatives --install /usr/bin/node node /usr/local/bin/node 1 && \
    rm -R node-v18.18.2-linux-x64

# NPM
RUN curl -L https://www.npmjs.com/install.sh | sh && \
    update-alternatives --install /usr/bin/npm npm /usr/local/lib/node_modules/npm/bin/npm-cli.js 1

# Yarn
RUN npm install --global yarn

#######################################################################################################################
# Prod build (Expects node packages to be installed)
#######################################################################################################################
FROM nginx as prod

# Apply default nginx client config.
COPY ./docker/client/nginx/conf.d/tuency.example.conf /etc/nginx/conf.d/default.conf

# Moving client source to webroot.
COPY ./client/dist /var/www/client

# Copy sample resources.
COPY ./docs/examples/tuency_resources/tuencyone/resources /var/www/resources
COPY ./docs/examples/tuency_resources/logo /var/www/resources/logo

# Adapting file permissions.
RUN chown -R www-data:www-data /var/www && \
    chmod -R 755 /var/www
