services:

  # Traefik Reverse Proxy
  # Config Docs: https://doc.traefik.io/traefik/v2.10/
  proxy:
    image: traefik:v2.10
    restart: unless-stopped
    networks:
      proxy-nw:
      internal-nw:
    ports:
      - "127.0.0.1:80:80"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ./docker/proxy/config/traefik.yml:/traefik.yml
      - ./docker/proxy/config/dynamic:/dynamic
      - ./docker/proxy/logs/:/logs
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=proxy-nw"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME}-proxy.entrypoints=http"
      - "traefik.http.services.${COMPOSE_PROJECT_NAME}-proxy.loadbalancer.server.port=8080"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME}-proxy.rule=Host(`$PROXY_URL`)"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME}-proxy.service=api@internal"

  # Laravel optimized NGINX with php and php-fpm
  backend:
    image: tuency-backend
    build:
      dockerfile: ./docker/backend/Dockerfile
      target: dev
    environment:
      - DISABLE_OPCACHE=true
    depends_on:
      database:
        condition: service_healthy
      keycloak:
        condition: service_healthy
    healthcheck:
      test: curl --fail http://localhost/api/uptest || exit 1
      interval: 20s
      timeout: 5s
      retries: 10
      start_period: 5s
    networks:
      internal-nw:
        aliases:
          - ${BACKEND_INTERNAL_ALIAS}
    volumes:
      - ./backend:/var/www
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=internal-nw"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME}-backend.entrypoints=http"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME}-backend.rule=Host(`$BACKEND_URL`)"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME}-backend.service=${COMPOSE_PROJECT_NAME}-backend"
      - "traefik.http.services.${COMPOSE_PROJECT_NAME}-backend.loadbalancer.server.port=80"

  client:
    image: tuency-client
    build:
      dockerfile: ./docker/client/Dockerfile
      target: dev
    depends_on:
      backend:
        condition: service_started
    networks:
      internal-nw:
    volumes:
      - ./client:/var/www/client
      - ./docs/examples/tuency_resources:/var/www/resources
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=internal-nw"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME}-client.entrypoints=http"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME}-client.rule=Host(`$CLIENT_ONE_URL`) || Host(`$CLIENT_TWO_URL`) || Host(`$CLIENT_THREE_URL`)"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME}-client.service=${COMPOSE_PROJECT_NAME}-client"
      - "traefik.http.services.${COMPOSE_PROJECT_NAME}-client.loadbalancer.server.port=80"

  # Keycloak
  # Server Docs: https://www.keycloak.org/server/all-config
  keycloak:
    image: quay.io/keycloak/keycloak:25.0.1
    command:
      - "start"
      - "--hostname-strict=false"
      - "--http-enabled=true"
      - "--spi-theme-static-max-age=-1"
      - "--spi-theme-cache-themes=false"
      - "--spi-theme-cache-templates=false"
    environment:
      # Keycloak
      - KEYCLOAK_ADMIN=${KEYCLOAK_ADMIN}
      - KEYCLOAK_ADMIN_PASSWORD=${KEYCLOAK_ADMIN_PASSWORD}
      - KC_DB=${KC_DB}
      - KC_DB_URL_HOST=${KC_DB_URL_HOST}
      - KC_DB_URL_DATABASE=${KC_DB_URL_DATABASE}
      - KC_DB_USERNAME=${KC_DB_USERNAME}
      - KC_DB_PASSWORD=${KC_DB_PASSWORD}
      - KC_HTTP_PORT=80
      - KC_HOSTNAME=${KEYCLOAK_URL}
      - KC_HEALTH_ENABLED=true
      # Tuency realm
      - TUENCY_REALM=${TUENCY_REALM}
      - TUENCY_REALM_ADMIN=${TUENCY_REALM_ADMIN}
      - TUENCY_REALM_ADMIN_PASSWORD=${TUENCY_REALM_ADMIN_PASSWORD}
    depends_on:
      database:
        condition: service_healthy
    healthcheck:
      test: [ "CMD-SHELL", "exec 3<>/dev/tcp/localhost/9000 && echo -e 'GET /health/ready HTTP/1.1\\r\\nHost: localhost\\r\\nConnection: close\\r\\n\\r\\n' >&3 && cat <&3 | grep -q '200 OK'" ]
      interval: 20s
      timeout: 5s
      retries: 20
      start_period: 20s
    networks:
      internal-nw:
        aliases:
          - ${KEYCLOAK_INTERNAL_ALIAS}
          - ${KEYCLOAK_URL}
    volumes:
      - ./keycloak_theme/tuency_kc_theme:/opt/keycloak/themes/tuency
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=internal-nw"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME}-keycloak.entrypoints=http"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME}-keycloak.rule=Host(`$KEYCLOAK_URL`)"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME}-keycloak.service=${COMPOSE_PROJECT_NAME}-keycloak"
      - "traefik.http.services.${COMPOSE_PROJECT_NAME}-keycloak.loadbalancer.server.port=80"

  database:
    image: postgres:16.0-bookworm
    environment:
      - POSTGRES_USER=${POSTGRES_ADMIN_USER}
      - POSTGRES_PASSWORD=${POSTGRES_ADMIN_PASSWORD}
      - POSTGRES_BACKEND_USER=${POSTGRES_BACKEND_USER}
      - POSTGRES_BACKEND_PASSWORD=${POSTGRES_BACKEND_PASSWORD}
      - POSTGRES_BACKEND_DB=${POSTGRES_BACKEND_DB}
      - POSTGRES_KEYCLOAK_USER=${POSTGRES_KEYCLOAK_USER}
      - POSTGRES_KEYCLOAK_PASSWORD=${POSTGRES_KEYCLOAK_PASSWORD}
      - POSTGRES_KEYCLOAK_DB=${POSTGRES_KEYCLOAK_DB}
    networks:
      internal-nw:
        aliases:
          - ${POSTGRES_INTERNAL_ALIAS}
    # Optionally map the postgres port to your host,
    # for example to reach the db over a local client.
    #ports:
    #  - "5432:5432"
    volumes:
      - postgres:/var/lib/postgresql/data
      - ./docker/database/setup-backend-db.sh:/docker-entrypoint-initdb.d/setup-backend-db.sh
      - ./docker/database/setup-keycloak-db.sh:/docker-entrypoint-initdb.d/setup-keycloak-db.sh
    healthcheck:
      test: [ "CMD-SHELL", "pg_isready", "-d", "db_prod" ]
      interval: 1s
      timeout: 5s
      retries: 60
      start_period: 20s

  mailhog:
    image: mailhog/mailhog
    restart: unless-stopped
    environment:
      - MH_SMTP_BIND_ADDR=0.0.0.0:25
    networks:
      internal-nw:
        aliases:
          - ${MAILHOG_INTERNAL_ALIAS}
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=internal-nw"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME}-mailhog.entrypoints=http"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME}-mailhog.rule=Host(`$MAILHOG_URL`)"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME}-mailhog.service=${COMPOSE_PROJECT_NAME}-mailhog"
      - "traefik.http.services.${COMPOSE_PROJECT_NAME}-mailhog.loadbalancer.server.port=8025"

  # Add this container to you local setup for
  # cypress e2e test development.
  # NOTE: This config was only tested for X11/Wayland in WSLg.
  # Other platforms may require additional configuration.
  #cypress:
  #  image: cypress/included:13.15.0 # Note 13.15.<1|2> are currently broken with Firefox 132 (headless)
  #  environment:
  #    - DISPLAY
  #    - HTTP_PROXY=http://proxy
  #  entrypoint: "cypress open --project /e2e"
  #  depends_on:
  #    backend:
  #      condition: service_started
  #  networks:
  #    internal-nw:
  #  working_dir: /e2e
  #  volumes:
  #    - ./docker/e2e:/e2e/
  #    - /tmp/.X11-unix:/tmp/.X11-unix

networks:
  proxy-nw:
    driver: bridge
    external: false
  internal-nw:
    external: false

volumes:
  postgres:
    driver: ${VOLUMES_DRIVER}